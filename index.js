var names = ["Gimli", "Fili", "Ilif", "Ilmig", "Mark"]

// Remove capital letter
var names = names.map(_ => _.toLowerCase())

for(var i = 0; i < names.length; i ++) {
  var first = names[i]
  // Loop all the others name in the array
  for(var k = i + 1; k < names.length; k++) {
    second = names[k]
    // If we have "Abc" and "Ba" it's palindrome only in this way, not the opposite one
    var firstSecond = first + second
    var secondFirst = second + first
    if(firstSecond === reverseString(firstSecond)) {
      console.log(capitalizeFirstLetter(first) + " " + capitalizeFirstLetter(second) + " are palindromes")
    }
    if(secondFirst === reverseString(secondFirst)){
      console.log(capitalizeFirstLetter(second) + " " + capitalizeFirstLetter(first) + " are palindromes")
    }
  }
}

function reverseString(str) {
  return str.split("").reverse().join("");
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}